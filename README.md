http://localhost:8001/api/v1/namespaces/tekton-pipelines/services/tekton-dashboard:http/proxy/#/pipelineruns
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/service/argocd/argocd-server?namespace=argocd

kubectl.exe get svc -n argocd
kubectl.exe port-forward -n argocd svc/argocd-server 8080:443

kubectl.exe get svc -n dspdf-nec2
kubectl.exe port-forward -n dspdf-nec2 svc/nec2-service 58889:8080